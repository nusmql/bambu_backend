var calculate = require('./saving-score');

describe('calculate', function () {

    it('should be calcuate score for input 2000', function () {
        expect(calculate(2000)).toBe(1);
    });

    it('should be calcuate score for input 4000', function () {
        expect(calculate(4000)).toBe(2);
    });

    it('should be calcuate score for input 6000', function () {
        expect(calculate(6000)).toBe(3);
    });

    it('should be calcuate score for input 8000', function () {
        expect(calculate(8000)).toBe(4);
    });

    it('should be calcuate score for input 10000', function () {
        expect(calculate(10000)).toBe(5);
    });
});