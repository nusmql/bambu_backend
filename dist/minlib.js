var fs = require('fs');
var compressor = require('node-minify');

var libdir = String(__dirname) + '/lib';
var libmindir = String(__dirname) + '/lib-min';

fs.readdirSync(libdir).filter(function (file) {
  return file.indexOf('.') !== 0 && file.slice(-3) === '.js' && !file.match(/-test/i);
}).forEach(function (file) {
  var filename = file.slice(0, -3);

  // use for google closure compiler
  compressor.minify({
    compressor: 'gcc',
    input: libdir + '/' + String(file),
    output: libmindir + '/' + String(filename) + '.min.js',
    callback: function () {
      function callback(err, min) {
        if (err) console.error(err);
      }

      return callback;
    }()
  });
});

process.exit(1);