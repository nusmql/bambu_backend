/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('question', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    uuid: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    type: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    data: {
      type: DataTypes.JSON,
      allowNull: false
    },
    calculator: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    tableName: 'question',
    timestamps: false,
    paranoid: false,
    underscored: true
  });
};