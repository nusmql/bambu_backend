/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    ic: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    profile_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    profile_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      defaultValue: ''
    }
  }, {
    tableName: 'user',
    timestamps: false,
    paranoid: false,
    underscored: true
  });
};