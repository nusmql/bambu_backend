var restify = require('restify');
var errors = require('restify-errors');
var joi = require('joi');
var moment = require('moment');
var uuid = require('uuid-v4');

var requireFromString = require('require-from-string');

var env = process.env.NODE_ENV || 'development';
var mainConfig = require('../config/main.json')[env];
var models = require('./models');
var sequelize = models.sequelize;
var Op = models.Sequelize.Op;

var server = restify.createServer(mainConfig.server);

// pre processing request
server.pre(restify.plugins.pre.userAgentConnection());

// initialize server plugins here
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.authorizationParser());
// server.use(restify.dateParser())
server.use(restify.plugins.queryParser({
  mapParams: false
}));
// server.use(restify.plugins.jsonp())
// server.use(restify.plugins.gzipResponse())
server.use(restify.plugins.bodyParser());

////////////////API////////////////
server.get('/questions', async function (req, res, next) {

  try {
    var questionsRaw = await models.question.findAll();
    var registerQuestions = [];
    var uid = uuid();

    var questions = questionsRaw.map(function (question) {
      registerQuestions.push({
        timestamp: moment().unix(),
        token: uid,
        question_id: question.id
      });

      return {
        uid: question.uuid,
        description: question.description,
        type: question.type,
        data: question.data
      };
    });

    var t = await sequelize.transaction();

    try {
      await models.user_question.bulkCreate(registerQuestions, {
        transaction: t,
        validate: true,
        individualHooks: true
      });

      await t.commit();
    } catch (err) {
      t.rollback();
      throw new Error('register question encounter error');
    }

    res.send(200, {
      code: 200,
      data: {
        token: uid,
        questions: questions
      }
    });
    return next();
  } catch (err) {
    console.error(err);

    res.json(422, {
      code: 422,
      error: err.message,
      request: req.getPath()
    });

    return next(false);
  }
});

server.post('/register', async function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');

  var questionSchema = joi.object().keys({
    uid: joi.string().guid({ version: ['uuidv4'] }).required(),
    answer: joi.string().trim().required()
  });

  var userSchema = joi.object().keys({
    token: joi.string().guid({ version: ['uuidv4'] }).required(),
    name: joi.string().required(),
    ic: joi.string().required(),
    email: joi.string().allow('').email().optional(),
    questions: joi.array().items(questionSchema).length(2).unique()
  });

  var result = joi.validate(req.body, userSchema);
  console.log(result);

  if (result.error) {
    console.log(result.error);
    var error = result.error.details[0];

    res.json(422, {
      code: 422,
      error: error,
      request: req.getPath()
    });

    return next(false);
  }

  var value = result.value;

  try {

    var userQuestions = await models.user_question.findAll({
      where: {
        token: value.token,
        status: 0
      },
      include: [models.question]
    });

    var count = userQuestions.length;

    if (count === 0) {
      res.json(422, {
        code: 422,
        error: 'invalid token',
        request: req.getPath()
      });

      return next(false);
    }

    var user = {
      create_time: moment().format('YYYY-MM-DD HH:mm:ss'),
      update_time: moment().format('YYYY-MM-DD HH:mm:ss'),
      name: value.name,
      ic: value.ic,
      email: value.email,
      profile_id: '',
      profile_name: ''
    };

    var t = await sequelize.transaction();

    try {
      var totalScore = 0;

      await Promise.all(userQuestions.map(async function (userQuestion) {
        var calculatorStr = userQuestion.question.calculator;
        var calculator = requireFromString(calculatorStr);
        var questionInput = value.questions.filter(function (q) {
          return q.token === userQuestion.question.token;
        })[0];
        var score = calculator(questionInput.answer);

        totalScore += score;

        return await models.user_question.update({
          status: 1,
          answer: questionInput.answer,
          score: score
        }, {
          where: {
            id: userQuestion.id
          },
          transaction: t
        });
      }));

      // console.log('final score',totalScore)

      switch (true) {
        case totalScore >= 8:
          user.profile_id = 1;
          user.profile_name = 'Profile A';
          break;
        case totalScore < 8 && totalScore >= 6:
          user.profile_id = 2;
          user.profile_name = 'Profile B';
          break;
        case totalScore < 6 && totalScore >= 4:
          user.profile_id = 3;
          user.profile_name = 'Profile C';
          break;
        case totalScore < 4 && totalScore >= 2:
          user.profile_id = 4;
          user.profile_name = 'Profile D';
          break;
        default:
          throw new Error('Invalid total score');
          break;
      }

      var userSaved = await models.user.create(user, {
        transaction: t,
        validate: true,
        individualHooks: true
      });

      await t.commit();

      res.json(200, {
        code: 200,
        data: userSaved
      });
    } catch (e) {
      console.log(e);
      t.rollback();
      throw new Error('update user question and save user encouter error');
    }

    return next();
  } catch (err) {
    console.log(err);

    res.json(422, {
      code: 422,
      error: err.message,
      request: req.getPath()
    });

    return next(false);
  }
});

//////////
server.listen(mainConfig.server.port, function (err) {
  if (err) console.error(err);
  console.error('%s listening at %s', server.name, server.url);
});