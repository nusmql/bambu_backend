const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

// relative import
const mainConfig = require('../../config/main')

const basename = path.basename(module.filename)
const env = process.env.NODE_ENV || 'development'
const config = mainConfig[env]
const db = {}

const sequelize = new Sequelize(config.mysql.database, config.mysql.username, config.mysql.password, config.mysql)

// import modified models
fs.readdirSync(__dirname)
    .filter((file) => (
        (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
    ))
    .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, file))
        db[model.name] = model
    })

// import unregistered base models
fs.readdirSync(path.join(__dirname, 'base'))
    .filter((file) => (
        (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
    ))
    .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, 'base', file))
        if (db[model.name] === undefined) {
            db[model.name] = model
        }
    })

Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
        db[modelName].associate(db)
    }
})

// data relationsp
db.question.hasMany(db.user_question, { foreignKey: 'question_id', sourceKey: 'id' })
db.user_question.belongsTo(db.question, { foreignKey: 'question_id', targetKey: 'id' })

db.profile.hasMany(db.user, { foreignKey: 'profile_id', sourceKey: 'id' })
db.user.belongsTo(db.profile, { foreignKey: 'profile_id', targetKey: 'id' })

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
