/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profile', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    criteria: {
      type: DataTypes.JSON,
      allowNull: false
    }
  }, {
    tableName: 'profile',
    timestamps: false,
    paranoid: false,
    underscored: true
  });
};
