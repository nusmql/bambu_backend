const fs = require('fs')
const compressor = require('node-minify')

const libdir = `${__dirname}/lib`
const libmindir = `${__dirname}/lib-min`

fs.readdirSync(libdir)
  .filter((file) => (
    (file.indexOf('.') !== 0) && (file.slice(-3) === '.js') && (!file.match(/-test/i))
  ))
  .forEach((file) => {
    let filename = file.slice(0, -3)

    // use for google closure compiler
    compressor.minify({
      compressor: 'gcc',
      input: `${libdir}/${file}`,
      output: `${libmindir}/${filename}.min.js`,
      callback: (err, min) => { if(err) console.error(err)},
    })
  })

process.exit(1)

