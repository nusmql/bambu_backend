function calculate(saving) {
    let score = null

    switch (parseInt(saving, 10)) {
        case 2000:
            score = 5
            break
        case 4000:
            score = 4
            break
        case 6000:
            score = 3
            break
        case 8000:
            score = 2
            break
        case 10000:
            score = 1
            break
        default:
            break
    }

    return score
}

module.exports = calculate