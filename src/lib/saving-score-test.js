const calculate = require('./saving-score')

describe('calculate', () => {

    it('should be calcuate score for input 2000', () => {
        expect(calculate(2000)).toBe(1)
    })

    it('should be calcuate score for input 4000', () => {
        expect(calculate(4000)).toBe(2)
    })

    it('should be calcuate score for input 6000', () => {
        expect(calculate(6000)).toBe(3)
    })

    it('should be calcuate score for input 8000', () => {
        expect(calculate(8000)).toBe(4)
    })

    it('should be calcuate score for input 10000', () => {
        expect(calculate(10000)).toBe(5)
    })


})