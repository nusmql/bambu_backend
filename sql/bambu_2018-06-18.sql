# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: bambu
# Generation Time: 2018-06-17 18:23:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `criteria` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;

INSERT INTO `profile` (`id`, `name`, `criteria`)
VALUES
	(1,'Profile A',X'7B2276616C7565223A20382C20226F70657261746F72223A20223E3D227D'),
	(2,'Profile B',X'7B22616E64223A205B7B2276616C7565223A20362C20226F70657261746F72223A20223E3D227D2C207B2276616C7565223A20382C20226F70657261746F72223A20223C227D5D7D'),
	(3,'Profile C',X'7B22616E64223A205B7B2276616C7565223A20342C20226F70657261746F72223A20223E3D227D2C207B2276616C7565223A20362C20226F70657261746F72223A20223C227D5D7D'),
	(4,'Profile D',X'7B22616E64223A205B7B2276616C7565223A20342C20226F70657261746F72223A20223E3D227D2C207B2276616C7565223A20322C20226F70657261746F72223A20223C227D5D7D');

/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL,
  `update_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` varchar(64) NOT NULL,
  `data` json NOT NULL,
  `calculator` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;

INSERT INTO `question` (`id`, `uuid`, `update_time`, `create_time`, `description`, `type`, `data`, `calculator`)
VALUES
	(1,'10c0c323-6e44-41f9-8aca-c19ab0601fbe','2018-06-17 09:00:00','2018-06-17 09:00:00','Your current saving amount','single',X'7B2232303030223A202230207E2032303030222C202234303030223A202232303030207E2034303030222C202236303030223A202234303030207E2036303030222C202238303030223A202236303030207E2038303030222C20223130303030223A202238303030207E203130303030227D','function calculate(b){var a=null;switch(parseInt(b,10)){case 2E3:a=1;break;case 4E3:a=2;break;case 6E3:a=3;break;case 8E3:a=4;break;case 1E4:a=5}return a}module.exports=calculate;'),
	(2,'87edca41-ff92-46c7-83aa-97bb041112f3','2018-06-17 09:10:10','2018-06-17 09:10:10','Your current loan amount','single',X'7B2232303030223A202230207E2032303030222C202234303030223A202232303030207E2034303030222C202236303030223A202234303030207E2036303030222C202238303030223A202236303030207E2038303030222C20223130303030223A202238303030207E203130303030227D','function calculate(b){var a=null;switch(parseInt(b,10)){case 2E3:a=5;break;case 4E3:a=4;break;case 6E3:a=3;break;case 8E3:a=2;break;case 1E4:a=1}return a}module.exports=calculate;');

/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table question_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question_type`;

CREATE TABLE `question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;

INSERT INTO `question_type` (`id`, `name`, `description`)
VALUES
	(1,'single','Single Choice'),
	(2,'mulitiple','Multiple Choice');

/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `ic` varchar(45) DEFAULT NULL,
  `profile_id` int(11) NOT NULL,
  `profile_name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `create_time`, `update_time`, `name`, `email`, `ic`, `profile_id`, `profile_name`)
VALUES
	(1,'2018-06-17 17:57:42','2018-06-17 17:57:42','Jack','','s82131230d',2,'Profile B');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_question`;

CREATE TABLE `user_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) NOT NULL COMMENT 'created timestampe in unixtimestapm format',
  `token` varchar(255) NOT NULL DEFAULT '' COMMENT 'uuid for imdemportent purpose',
  `question_id` int(11) NOT NULL,
  `status` int(8) NOT NULL DEFAULT '0' COMMENT '0 pending, 1 submited',
  `answer` varchar(255) NOT NULL DEFAULT '' COMMENT 'submitted answer',
  `score` int(11) DEFAULT NULL COMMENT 'calcuated score',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_question_question_id_session_unique` (`token`,`question_id`),
  KEY `user_question_question_id_index` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_question` WRITE;
/*!40000 ALTER TABLE `user_question` DISABLE KEYS */;

INSERT INTO `user_question` (`id`, `timestamp`, `token`, `question_id`, `status`, `answer`, `score`)
VALUES
	(1,1529251393,'9c781a5b-70c6-41e7-bfc1-e7b664969599',1,1,'4000',2),
	(2,1529251393,'9c781a5b-70c6-41e7-bfc1-e7b664969599',2,1,'4000',4),
	(3,1529253126,'aff75e62-4a08-409d-9ea4-52fe29cd5ca3',1,0,'',NULL),
	(4,1529253126,'aff75e62-4a08-409d-9ea4-52fe29cd5ca3',2,0,'',NULL);

/*!40000 ALTER TABLE `user_question` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
