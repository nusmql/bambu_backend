# BamBu Backend Test

# Database related
The database use MySQL database. Base sql file in the sql folder. please import base db into local.

```
mysql -uxxxx -p
CREATE DATABASE bambu CHARACTER SET utf8 COLLATE utf8_general_ci;

// from terminal 
mysql -uxxxx -p bambu < bambu_2018-6-18.sql
```
# Installation

You need run below command to install all the dependency packages
```
$ npm install
```

# Unit Test for library 
use jest as test library to conduct unit testing.
```
$ npm run test
```

# Deployment
You need install pm2 in production environment.
```
$ pm2 process.yml --env=production 
```
